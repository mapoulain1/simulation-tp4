#include "RabbitSimulator.h"
#include <vector>

#include "Rabbit.h"
#include "Parameters.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include "Generator.h"
#include "Utils.h"


using namespace std;

int RabbitSimulator::simulateFibonacci(int months)
{
	int first = 0;
	int second = 1;
	int tmp;

	for (int i = 0; i < months; i++)
	{
		tmp = first + second;
		first = second;
		second = tmp;
	}

	return first;
}


vector<int> RabbitSimulator::simulateAdvenced(int months, int initialRabbits, Parameters& param, string filename)
{
	vector<Rabbit> rabbits(100000000);
	vector<int> history = vector<int>(months);
	int numberOfRabbits = initialRabbits;
	int numberTotalOfNewKitten;
	int numberTotalOfMales;
	int numberTotalOfKittens;
	float loveForce = 1;
	float attackForce = 0;
	ofstream file;
	if (!filename.empty()) {
		file.open(filename);
		cout << fixed;
		cout << setprecision(3);
		cout << "Month\tFemale\tMale\tNew\tKitten\tTotal\tLove\tAttack" << endl;
		file << "Month\tFemale\tMale\tNew\tKitten\tTotal\tLove\tAttack" << endl;
	}

	for (int i = 0; i < initialRabbits; i++)
	{
		rabbits[i] = Rabbit(param);
	}

	for (int i = 0; i < months; i++)
	{

		if (param.enableLove) {
			loveForce = Generator::normal(1.0, 0.7);
		}
		if (param.enableAttacks) {
			attackForce = Generator::normal(-0.7, 0.45);
		}

		numberTotalOfMales = 0;
		numberTotalOfNewKitten = 0;
		numberTotalOfKittens = 0;

		for (int j = 0; j < numberOfRabbits; j++) {
			numberTotalOfNewKitten += rabbits[j].update(loveForce, attackForce);
		}

		for (int j = 0; j < numberOfRabbits; j++) {
			if (rabbits[j].isDead()) {
				rabbits[j] = rabbits[numberOfRabbits - 1];
				numberOfRabbits--;
			}
		}

		for (int j = 0; j < numberTotalOfNewKitten; j++)
		{
			rabbits[numberOfRabbits] = Rabbit(param);
			numberOfRabbits++;
		}

		for (int j = 0; j < numberOfRabbits; j++) {
			if (rabbits[j].getGender() == Gender::MALE) {
				numberTotalOfMales++;
			}
			numberTotalOfKittens += rabbits[j].isKitten() ? 1 : 0;
		}



		history[i] = numberOfRabbits;
		if (!filename.empty()) {
			cout << i << "\t" << numberOfRabbits - numberTotalOfMales << "\t" << numberTotalOfMales << "\t" << numberTotalOfNewKitten << "\t" << numberTotalOfKittens << "\t" << numberOfRabbits << "\t" << loveForce << "\t" << attackForce << endl;
			file << i << "\t" << numberOfRabbits - numberTotalOfMales << "\t" << numberTotalOfMales << "\t" << numberTotalOfNewKitten << "\t" << numberTotalOfKittens << "\t" << numberOfRabbits << "\t" << loveForce << "\t" << attackForce << endl;
		}
	}





	file.close();
	return history;
}

vector<vector<int>> RabbitSimulator::simulateAdvencedN(int n, int months, int initialRabbits, Parameters& param)
{
	vector<vector<int>> histories(n, vector<int>(months));
	for (int i = 0; i < n; i++)
	{
		cout << ((double)i / n) * 100 << "%" << endl;
		histories[i] = simulateAdvenced(months, initialRabbits, param, "");
	}

	return histories;
}

void RabbitSimulator::exportSimulation(vector<vector<int>> simulation, string filename)
{
	int n = simulation.size();
	int months = simulation[0].size();
	double deviation;
	double mean;
	int min;
	int max;
	ofstream file;
	file.open(filename);
	file << "month\tmin\tlower95\taverage\tupper95\tmax" << endl;

	for (int i = 0; i < months; i++)
	{
		vector<int> current(n);
		for (int j = 0; j < n; j++)
		{
			current[j] = simulation[j][i];
		}
		Utils::standardDeviation(current, &deviation, &mean, &min, &max);
		file << i << "\t" << min << "\t" << mean - 1.96 * (deviation / sqrt(n)) << "\t" << mean << "\t" << mean + 1.96 * (deviation / sqrt(n)) << "\t" << max << "\t" << endl;
	}
	file.close();
	cout << "Saved to " << filename << endl;

}


