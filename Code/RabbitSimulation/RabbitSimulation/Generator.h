#pragma once
#include <random>

class Generator
{
private:
	static std::mt19937 generator;
	static std::uniform_real_distribution<double> uniformDistribution;
	static std::normal_distribution<double> normalDistribution;

public: 
	
	static void initialize(std::seed_seq seq);
	static void initialize();
	static double random();
	static double random(double lower, double upper);
	static double normal();
	static double normal(double mean, double sigma);

};

