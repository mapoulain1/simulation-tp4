#pragma once
#include <vector>
using namespace std;

class Utils
{
public:
	static void standardDeviation(vector<int> result, double* outDeviation, double* outMean, int* min, int* max);
};

