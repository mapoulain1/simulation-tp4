#pragma once
#include "Gender.h"
#include "Parameters.h"


#define RABBIT_YOUNG_AGE 4
#define RABBIT_ADULT_AGE 120
#define RABBIT_OLD_AGE 180


#define RABBIT_YOUNG_DEATH_RATIO 0.45
#define RABBIT_ADULT_DEATH_RATIO 0.40
#define RABBIT_OLD_DEATH_RATIO_STEP 0.10


class Rabbit
{
private:
	short age;
	Gender gender;
	float litters;
	bool alive;

	void deadOrAlive(float attackForce);
	int pregnancy(float loveForce);


public:
	Rabbit();
	Rabbit(Parameters& param);
	int update(float loveForce, float attackForce);
	bool isDead();
	bool isKitten();
	short getAge();
	Gender getGender();


};

