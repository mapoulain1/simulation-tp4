#include <iostream>
#include "RabbitSimulator.h"
#include "Rabbit.h"
#include "Generator.h"
#include "Parameters.h"
#include <chrono>


using namespace std;

void testSimulationN() {
	Parameters p{ 0.5 , true, true};
	Generator::initialize();

	auto t_start = std::chrono::high_resolution_clock::now();
	auto simulation = RabbitSimulator::simulateAdvencedN(100, 240, 1000, p);
	auto t_end = std::chrono::high_resolution_clock::now();

	double elapsed_time_ms = std::chrono::duration<double, std::milli>(t_end - t_start).count();
	cout << elapsed_time_ms << "ms" << endl;
	RabbitSimulator::exportSimulation(simulation,"multipleSimulation.tsv");
}




void testSimulation() {
	Parameters p{ 0.5 , true, true };
	Generator::initialize();

	auto t_start = std::chrono::high_resolution_clock::now();
	RabbitSimulator::simulateAdvenced(240, 10000, p, "");
	auto t_end = std::chrono::high_resolution_clock::now();

	double elapsed_time_ms = std::chrono::duration<double, std::milli>(t_end - t_start).count();
	cout << elapsed_time_ms << "ms" << endl;
}


void usage() {
	cout << "usage: RabbitSimulation.exe [numberOfSimulation] [months] [initialPopulation] [outputFile]" << endl;
}


int main(int argc, char *argv[])
{
	if (argc != 5) {
		usage();
		getc(stdin);
		return -1;
	}
	int numberOfSimulation = atoi(argv[1]);
	int numberMonths = atoi(argv[2]);
	int initialPopulation = atoi(argv[3]);
	Parameters p{ 0.5 , true, true };
	Generator::initialize();


	auto t_start = std::chrono::high_resolution_clock::now();
	auto simulation = RabbitSimulator::simulateAdvencedN(numberOfSimulation, numberMonths, initialPopulation, p);
	auto t_end = std::chrono::high_resolution_clock::now();


	cout << std::chrono::duration<double, std::milli>(t_end - t_start).count() << "ms" << endl;
	RabbitSimulator::exportSimulation(simulation,string(argv[4]));
	getc(stdin);
}


