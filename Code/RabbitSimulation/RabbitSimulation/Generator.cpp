#include "Generator.h"


std::mt19937 Generator::generator;
std::uniform_real_distribution<double> Generator::uniformDistribution;
std::normal_distribution<double> Generator::normalDistribution;



void Generator::initialize(std::seed_seq seq)
{
	generator = std::mt19937(seq);
	uniformDistribution = std::uniform_real_distribution<double>(0, 1);
	normalDistribution = std::normal_distribution<double>(0.0, 1.0);
}

void Generator::initialize()
{
	initialize({ 0x143, 0x224, 0x345, 0x466 });
}

double Generator::random()
{
	return uniformDistribution(generator);
}

double Generator::random(double lower, double upper)
{
	return uniformDistribution(generator) * (upper - lower) + lower;
}

double Generator::normal()
{
	return normalDistribution(generator);
}

double Generator::normal(double mean, double sigma)
{
	return normal() * sigma + mean;
}



