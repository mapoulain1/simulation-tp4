#include "Utils.h"

#include <numeric>

using namespace std;

void Utils::standardDeviation(vector<int> result, double *outDeviation, double *outMean, int *min, int *max) {
	double sum = 0;
	int size = result.size();
	float average = accumulate(result.begin(), result.end(), 0.0) / size;
	*min = result[0];
	*max = result[0];

	for (int i = 0; i < size; i++)
	{
		sum += (result[i] - average) * (result[i] - average);
		if (result[i] < *min)
			*min = result[i];
		if (result[i] > *max)
			*max = result[i];
	}
	*outDeviation = sqrt(sum / (size - 1));
	*outMean = average;
}
