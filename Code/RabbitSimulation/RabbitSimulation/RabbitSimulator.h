#pragma once

#include "Parameters.h"
#include <string>
#include <vector>

using namespace std;

class RabbitSimulator
{
public:
	static int simulateFibonacci(int months);
	static vector<int> simulateAdvenced(int months, int initialRabbits, Parameters& param, string filename);
	static vector<vector<int>> simulateAdvencedN(int n, int months, int initialRabbits, Parameters& param);
	static void exportSimulation(vector<vector<int>> simulation, string filename);
};

