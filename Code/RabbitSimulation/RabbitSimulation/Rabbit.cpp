#include "Rabbit.h"
#include "Gender.h"
#include "Generator.h"
#include <iostream>

using namespace std;

void Rabbit::deadOrAlive(float attackForce)
{
	if (Generator::random() < attackForce) {
		alive = false;
		return;
	}

	if (age < RABBIT_YOUNG_AGE && Generator::random() < RABBIT_YOUNG_DEATH_RATIO) {
		alive = false;
		return;
	}

	if (age >= RABBIT_YOUNG_AGE && age < RABBIT_ADULT_AGE && Generator::random() < RABBIT_ADULT_DEATH_RATIO) {
		alive = false;
		return;
	}

	if (age >= RABBIT_ADULT_AGE && age < RABBIT_OLD_AGE && Generator::random() < RABBIT_ADULT_DEATH_RATIO - (((age - (short)RABBIT_ADULT_AGE) / 12.0) * RABBIT_OLD_DEATH_RATIO_STEP)) {
		alive = false;
		return;
	}

	if (age > RABBIT_OLD_AGE) {
		alive = false;
		return;
	}

}

int Rabbit::pregnancy(float loveForce)
{
	if (gender == Gender::MALE || age <= RABBIT_YOUNG_AGE) {
		return 0;
	}


	int oldLitters = litters;
	int kittens = 0;
	litters += (float)fabs(Generator::normal(8, 2) / 12.0);

	for (int i = oldLitters; i < litters; i++)
	{
		kittens += (int)Generator::random(4, 8);
	}

	return kittens * loveForce;
}





Rabbit::Rabbit() : age(0), gender(Gender::FEMALE), litters(0), alive(true)
{
}

Rabbit::Rabbit(Parameters& param)
	: age(0), gender(Gender::FEMALE), litters(0), alive(true)
{
	if (Generator::random() < param.maleFemaleRatio) {
		gender = Gender::MALE;
	}

}

int Rabbit::update(float loveForce, float attackForce)
{
	age++;
	deadOrAlive(attackForce);
	return pregnancy(loveForce);
}

bool Rabbit::isDead()
{
	return !alive;
}

bool Rabbit::isKitten()
{
	return age < RABBIT_YOUNG_AGE;
}

short Rabbit::getAge()
{
	return age;
}

Gender Rabbit::getGender()
{
	return gender;
}
