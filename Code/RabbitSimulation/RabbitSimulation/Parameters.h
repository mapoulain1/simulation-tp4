#pragma once

class Parameters {
public:
	double maleFemaleRatio;
	bool enableLove;
	bool enableAttacks;
	Parameters(double _maleFemaleRatio, bool _enableLove, bool _enableAttacks);
};
