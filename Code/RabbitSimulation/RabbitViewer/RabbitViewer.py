
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt



rabbits = pd.read_csv("rabbits.csv")[['Male\t','Female\t']]

nb_month = np.int_(rabbits.size/2).item()



ax = rabbits.plot.bar(stacked=True)
ax.set_ylabel('Rabbits')
ax.set_xlabel('Months')

plt.show()

